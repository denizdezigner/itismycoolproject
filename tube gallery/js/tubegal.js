$(function(){
	if($('.watch').length){
		var videos = [];
		var singles = false;
		var multi = false;
		var first = 0;
		var next = first+1;
		var inited = false;
		var vidHidden = true;
		var slInited = false;
		var type = 0;

		$('.watch').eq(0).before('<div id="wrap"><noscript><p>Включите <i>javascript</i></p></noscript><a href="#" class="morevideos">next</a><div class="slidebtn btn-left">◄</div><div class="slidebtn btn-right">►</div><div id="slider" class="slider-horizontal"></div></div>');

		$.each($('.watch'), function(i, item){
			var src = '';

			//if iframes is used
			if($(this).attr('src')){
				var srctxt = $(this).attr('src');
				if( /playlist=/.test(srctxt.toString()) ){
					singles = true;
					var vIDs = srctxt.toString().match(/playlist=([^&\?#]+)[&\?]?/i);
					vIDs = vIDs[1].split(/\s*,\s*/);
					$.each(vIDs, function(j, jtem){
						//Generate api links array
						videos.push('https://gdata.youtube.com/feeds/api/videos/'+jtem+'?callback=?&v=2&alt=jsonc');//&format=5
					});
				}else{
					//src = 'playlists/'+srctxt.toString().match(/(?:=PL)[^&\?#]{14,23}/).toString().slice(3)+'?max-results=25&';
					//src = srctxt.toString().match(/=PL([^&\?]+)[&\?#]?/);
					src = srctxt.toString().match(/[?&]list=PL([^&\?#]+)[&\?]?/);
					src = 'playlists/'+src[1]+'?max-results=25&';
				}
			}

			//if objects is used
			if($(this).children('param[name=movie]').length){
				singles = true;
				//src = 'videos/'+$(this).children('param[name=movie]').attr('value').toString().match(/(?:\/v\/)[^&\?#]{8,14}/).toString().slice(3)+'?';
				src = $(this).children('param[name=movie]').attr('value').toString().match(/\/v\/([^&\?#]+)\??/i);
				src = 'videos/'+src[1]+'?';
			}

			//if anchors tags is used
			if( $(this).attr('href') ){
				if ( /[?&]list=PL/.test(item.toString()) ) {
					src = item.toString().match(/=PL([^&\?#]+)[&\?]?/);
					src = 'playlists/'+src[1]+'?max-results=25&';
				}else{
					singles = true;
					//src = 'videos/'+item.toString().match(/(?:v=)[^&\?#]{8,14}/).toString().slice(2)+'?';
					src = item.toString().match(/v=([^&\?#]+)&?/i);
					src = 'videos/'+src[1]+'?';
				}
			}

			//Generate api links array
			if(src){
				videos.push('https://gdata.youtube.com/feeds/api/'+src+'callback=?&v=2&alt=jsonc');//&format=5
			}

			//Remove static tags
			$(this).remove();
		});

		if(videos.length > 1){ multi = true; }

		function loadThumbs(url){
			$('#wrap').addClass('loading');
			$('#slider .www_FlowSlider_com-wrap-1').fadeOut('fast');

			// Stop and closing player
			if (!$('#video .stop').length){
				var Stop = function(){
					$('#video').tubeplayer('stop').animate({opacity:'0'},400, function(){$(this).css({'left':'-1000em','visibility':'collapse'});});
					$('#modal').fadeOut('fast');
					$('.morevideos').css({'color':'inherit'});
					vidHidden = true;
					return false;
				};
				$('<a href="#" class="stop">Stop</a>').prependTo($('#video')).click(function(e){ Stop(); e.preventDefault(); });
				$('#modal').click(function(){ Stop(); });
			}

			if(!singles){
				$.getJSON(url, function(response){
					if(!slInited){
						initSlider();
						$('#slider .www_FlowSlider_com-wrap-1').css('display','none');
						slInited = true;
					}

					//CLEAN SLIDER BEFORE FILL
					$('#slider .item').remove();

					var items = response.data.items;

					//CICLE
					$.each(items, function(i,item){
						if(items[1].video){
							var thumb = item.video.thumbnail.sqDefault;
							var title = item.video.title;
							var vid = item.video.id;
							var link = 'http://www.youtube.com/watch?v='+vid;
						}
						else{
							var thumb = item.thumbnail.sqDefault;
							var title = item.title;
							var vid = item.id;
							var link = 'http://www.youtube.com/watch?v='+vid;
						}

						//POPULATE THUMBS
						var $item = $('<div class="item"><a href="'+link+'" target="_blank" id="'+vid+'"><img alt="" src="'+thumb+'" />'+title+'<span class="animall"><span></span></span></a></div>');
						//$('#slider').append($item);
						$("#slider").FlowSlider().content().append($item);
						$("#slider").FlowSlider().setupDOM();
					});//END CICLE

					$('#slider .www_FlowSlider_com-wrap-1').fadeIn('slow'), $('#wrap').removeClass('loading');
				}).
				error(function(){
					jsonErr();
					return false;
				});
			}else{
				if(!slInited){
					initSlider();
					$('#slider .www_FlowSlider_com-wrap-1').css('display','none');
					slInited = true;
				}

				//CICLE
				$.each(url, function(i, item){
					$.getJSON(item, function(response){
						var thumb = response.data.thumbnail.sqDefault;
						var title = response.data.title;
						var vid = response.data.id;
						var link = 'http://www.youtube.com/watch?v='+vid;

						//POPULATE THUMBS
						var $item = $('<div class="item"><a href="'+link+'" target="_blank" id="'+vid+'"><img alt="" src="'+thumb+'" />'+title+'<span class="animall"><span></span></span></a></div>');
						//$('#slider').append($item);
						$("#slider").FlowSlider().content().append($item);
						$("#slider").FlowSlider().setupDOM();
					}).
					error(function(){
						jsonErr();
						return false;
					});
				});//END CICLE

				$('#slider .www_FlowSlider_com-wrap-1').fadeIn('slow'), $('#wrap').removeClass('loading');
			}
		};

		//INIT LOAD
		if(!singles){
			loadThumbs(videos[first]);
		}else{
			loadThumbs(videos);
		}

		//SHOW next BUTTON
		if(!singles && multi){
			$('.morevideos').css('display','inline').click(function(e){
				loadThumbs(videos[next]);
				next++;
				if(next == videos.length){next = 0}
				e.preventDefault();
			});
		}

		//FIRE PLAYER
		$('#slider .item a').live('click', function(){
			var vID = $(this).attr('id');
			if(!inited){
				$('#video').tubeplayer({
					width: 720,//640
					height: 480,//390
					initialVideo: vID
				}).
				css({'left':'50%','visibility':'visible'}).animate({opacity:'1'},600);
				$('#modal').fadeIn('slow');
				$('.morevideos').css({'color':'#fff'});
				inited = true;
				vidHidden = false;
				$('#video').tubeplayer('play', vID);
				return false;
			}else{
				if(vidHidden){
					$('#video').tubeplayer('play', vID).
					css({'left':'50%','visibility':'visible'}).animate({opacity:'1'},600);
					$('#modal').fadeIn('slow');
					$('.morevideos').css({'color':'#fff'});
					vidHidden = false;
				}else{
					$('#video').tubeplayer('play', vID);
				}
				return false;
			}
		});

		initNAvButts();
	}

	if($('#slider.hot').length){
		// set up player
		jwplayer("container").setup({
			flashplayer: "jwplayer/player.swf",
			height: 340,
			width: 500,
			file: $('#slider .item a').attr('href'),
			//image: "jwplayer/eyebig.jpg",
			skin: "jwplayer/glow.zip"
		});

		$("#slider .item a").click(function(e){
			playlist = { file: $(this).attr('href'), image:""}
			jwplayer("container").load(playlist).play();
			e.preventDefault();
		});

		initSlider();
		initNAvButts();
	}
});

//INIT SLIDER NAV BUTTONS
function initNAvButts(){
	$(".slidebtn").hover(function(){
		$("#slider").prop("FlowSlider")
			.stop()
			.set({
				controllers: ["Event", "Event"],
				controllerOptions: [
					{
						el: ".btn-right",
						step: 450
					},
					{
						el: ".btn-left",
						step: -450
					}
				]
			})
			.start();
	}, function(){
		$("#slider").prop("FlowSlider")
			.stop()
			.set({
				controllers: ["Hover"],
				controllerOptions: []
			})
			.start();
	});
}

//INIT SLIDER
function initSlider(){
	$("#slider").FlowSlider({
		detectTouchDevice: true,
		detectCssTransition: true,
			cssAnimationOptions: {
				transition: "cubic-bezier(0.05, 0, 0.54, 1.0)",
				time: 800
			},
		animation: "Transition",
			animationOptions: {
				transition: new FlowSlider.Transition
					.CubicBezier(0.07, 0.82, 0.54, 1.0).transition,
				minTime: 800,
				maxTime: 800
			},
		marginStart: 0,
		marginEnd: 0,
		position: 0.0
	});
}

//On json error
function jsonErr(){
	if(!$('#wrap').children('.err')){
		$('#wrap').prepend('<p class="err">Запрос видео не удался :(</p>');
	}
	console.log('$.getJSON().error("Запрос видео не удался :(")');
}
