jQuery.fn.center = function(){
	//this.css("position","absolute");
	this.css("top", (($(window).height() - this.height()) / 2+$(window).scrollTop()) + "px");//
	this.css("left", (($(window).width() - this.width()) / 2+$(window).scrollLeft()) + "px");//
	return this;
};
//INIT SLIDER
function initslider(){
	$("#slider").FlowSlider({
		detectTouchDevice: true,
		detectCssTransition: true,
			cssAnimationOptions: {
				transition: "cubic-bezier(0.05, 0, 0.54, 1.0)",
				time: 800
			},
		animation: "Transition",
			animationOptions: {
				transition: new FlowSlider.Transition
					 .CubicBezier(0.07, 0.82, 0.54, 1.0).transition,
				minTime: 800,
				maxTime: 800
			},
		marginStart: 0,
		marginEnd: 0,
		position: 0.0
	});
	$('.www_FlowSlider_com-wrap-1').fadeIn('slow'), $('#wrap').removeClass('loading');
}

$(function(){

	var videos = window.$videos;//['https://gdata.youtube.com/feeds/api/playlists/F73AA0280B41006C?callback=?&v=2&alt=jsonc&max-results=50','http://gdata.youtube.com/feeds/api/users/myducksvision/uploads?callback=?&v=2&alt=jsonc&format=5&max-results=25','http://gdata.youtube.com/feeds/api/users/ghost134699/favorites?callback=?&v=2&alt=jsonc&format=5&max-results=25'];
	var lnk = 0;
	var next = lnk+1;
	var initd = false;
	var vhidden = true;

	function loadvideos(url){
		//$('#wrap').ajaxStart(function(){
			//var haslist = $('#slider .item');
			//if(haslist){
			//	$('.www_FlowSlider_com-wrap-1').fadeOut('fast', function(){ $('#slider').addClass('loading'); });
			$('#wrap').addClass('loading');
			//};
			//$(this).addClass('loading');
			
			//console.log("$('#wrap').addClass('loading')");
		//});

		$.getJSON(url, function(response){
			
			// make stop and closing player
			if (!$('#video').children('.stop').length){
				var Stop = function(){
					$('#video').tubeplayer('stop').animate({opacity:'0'},400, function(){$(this).css({'left':'-1000em','visibility':'collapse'});});
					$('#modal').fadeOut();
					//$('#slider, #slider .ie7').css({'background-color':'#fff'});
					//$('#morevideos').css({'color':'#525252'});
					vhidden = true;
					return false;
				};
				$('<a href="javascript:;" class="stop">Stop</a>').prependTo($('#video')).click(function(){ Stop(); });
				$('#modal').click(function(){ Stop(); });
			}

			$('#slider .item').remove();
			var items = response.data.items;
			//CICLE
			$.each(items, function(i,item){
				if(items[1].video){
					var thumb = items[i].video.thumbnail.sqDefault;
					var title = items[i].video.title;
					var vid = items[i].video.id;
					var link = 'http://www.youtube.com/watch?v='+items[i].video.id;
				}
				else{
					var thumb = items[i].thumbnail.sqDefault;
					var title = items[i].title;
					var vid = items[i].id;
					var link = 'http://www.youtube.com/watch?v='+items[i].id;
				}
				
				//POPULATE THUMBS
				var $item = $('<div class="item"><a href="'+link+'" target="_blank"><img src="'+thumb+'" />'+title+'</a></div>');
				$('#slider').append($item);
				//.children('.www_FlowSlider_com-wrap-1').fadeIn('slow', function(){ $('#slider').removeClass('loading') })
					//if ( i == 10 ) return false;// УРЕЗКА
					
				
				//FIRE PLAYER
				$item.children('a').click(function(){
					if(!initd){
						$('#video').tubeplayer({
							width: 720,
							height: 480,
							initialVideo: vid,
							allowFullScreen: false
						}).
						css({'left':'50%','visibility':'visible'}).animate({opacity:'1'},600);
						$('#modal').fadeIn('slow');
						//$('#slider, #slider .ie7').css({'background-color':'#000'});
						//$('#morevideos').css({'color':'#fff'});
						initd = true;
						vhidden = false;
						$('#video').tubeplayer('play', vid);
						return false;
					}else{
						if(vhidden){
							$('#video').tubeplayer('play', vid).
							css({'left':'50%','visibility':'visible'}).animate({opacity:'1'},600);
							$('#modal').fadeIn('slow');
							//$('#slider, #slider .ie7').css({'background-color':'#000'});
							//$('#morevideos').css({'color':'#fff'});
							vhidden = false;
						}else{
							$('#video').tubeplayer('play', vid);
						}
						return false;
					}
				});
			});
			
		}).
		complete(function(){
			//$('#wrap').removeClass('loading');
			initslider();
		}).
		error(function(){
			if(!$('#slider').children('.err').length){
				$('#slider').prepend('<p class="err">Запрос видео не удался :(</p>');
			}
			console.log('$.getJSON().error("Запрос видео не удался")');
			return false;
		});
	};

	loadvideos(videos[lnk]);

	//$('#morevideos').click(function(){
	//	loadvideos(videos[next]);
	//	next = next+1;
	//	if(next == videos.length){next = 0}
	//});
	
	
	
	
	
				//NAV BUTTONS
				$(".slidebtn").hover(function(){
					$("#slider").prop("FlowSlider")
						.stop()
						.set({
							controllers: ["Event", "Event"],
							controllerOptions: [
								{
									el: ".btn-right",
									step: 450
								},
								{
									el: ".btn-left",
									step: -450
								}
							]
						})
						.start();
				}, function(){
					$("#slider").prop("FlowSlider")
						.stop()
						.set({
							controllers: ["Hover"],
							controllerOptions: []
						})
						.start();
				});
});