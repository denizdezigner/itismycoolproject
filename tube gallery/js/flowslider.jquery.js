var FlowSlider=function(e,u,g){
	var s="function",
		 y='<div class="',
		 r="absolute",
		 h=100,
		 q="horizontal",
		 p="left",
		 n="height",
		 o="width",
		 d=this;
	if("string"===typeof e)
		return jQuery(e).FlowSlider(u);
		
	if("object"===typeof e&&e.FlowSlider)return e.FlowSlider;
	if("array"===typeof e)return new FlowSlider.Array(e);
	var b=d,
		 c=g(e),
		 m,
		 j,
		 k,
		 i,
		 l,
		 e={
			size:o,
			sizeC:n,
			edge:p,
			edgeEnd:"right",
			edgeC:"top",
			mouse:"pageX",
			fOuterSize:function(a,b){
			return a.outerWidth(b)},
			fSize:function(a,b){
				return a.width(b)},
			fSizeC:function(a,b){
				return a.height(b)
			}
		 },
		 f={
			size:n,
			sizeC:o,
			edge:"top",
			edgeEnd:"bottom",
			edgeC:p,
			mouse:"pageY",
			fOuterSize:function(a,b){
				return a.outerHeight(b)
			},
			fSize:function(a,b){
				return a.height(b)
			},
			fSizeC:function(a,b){
				return a.width(b)
			}
		 },
		 w,
		 t,
		 v,
		 x,
		 a=g.extend( {}, window.FlowSlider.defaults, u );
		 
		 d.calcSize=function(){
			return this.wrapSize=b.props.fSize(j)
		};
		 d.getPosition=function(){
			return(a.marginStart-this.getOffset())/(a.marginStart-(b.props.fSize(c)-this.calcSize()-a.marginEnd))
		 };
		 d.position=function(a){
			0 > a && (a=0);
			1 < a && (a=1);
			this.offset(this.positionToOffset(a));
			return this
		 };
		 d.positionToOffset=function(d){
			return a.marginStart - d*( a.marginStart - (b.props.fSize(c) - this.calcSize() - a.marginEnd ) )
		 };
		 d.getOffset=function(){
			return l.getOffset()
		 };
		 d.offset=function(a){
			v(a);return this
		 };
		 d.boundOffset=function(a){
			return x(a)
		 };
		 
	var D=function(d){
			(a.moveIfSmaller||!(b.calcSize()<=b.props.fSize(c)))&&l.offset(d)
		 },
		 C=function(d){
			if(d>a.marginStart)
				return a.marginStart;
				var e=b.props.fSize(c)-b.calcSize()-a.marginEnd;
				return d<e?e:d
		 },
		 E=function(a){
			var c = l.getOffset();
			half_size = b.calcSize()/2;
			a > half_size && (a %= half_size);
			var d = c + half_size-a,
			c = Math.abs(c-a) < Math.abs(d) ? a : a-half_size,a=C(a);
			a != c && (l.shift(a > c ? half_size : -half_size), c += a > c ? half_size : -half_size);
			l.offset(c)
		 },
		 F=function(a){return a};
		 
		 d.move = function(a){
			return b.offset(b.getOffset()+a)
		 };
		 d.moveBounded=function(a){
			return b.offset(b.boundOffset(b.getOffset()+a))
		 };
		 d.setupDOM=function(){
			k=j.children();
			k.addClass(a.prefix+"-item");
			c.css({
				overflow:"hidden",
				position:"relative"
			});
			k.css({
				"float":p
			});
			if((a.mode=q) && !parseInt(c.css(n))){
				for( var i=k.length, h=0, e=0, d, f=0;  f < i;  f++ )
					$item=g(k[f]),d=parseInt($item.css("margin-top")),
					isNaN(d) && (d=0),
					e =d,
					d =parseInt($item.outerHeight()),
					isNaN(d) && (d=0),
					e +=d,
					d = parseInt( $item.css( "margin-bottom" ) ),
					isNaN(d) && (d=0),
					e += d,
					e > h && (h=e);
					c.css( n,h )
			}
			q == a.mode && b.props.fSize( m, 9999999 );
			a.infinite ? (k.clone(true).appendTo(j),v=E,x=F) : (v=D,x=C);
			return this
		 };
		 d.content = function(){ return j };
		 d.set=function(b){
			g.extend(a,b);
			this.s=a;
			z();
			return this
		 };
		 d.start = function(){
			for( var a=0; a < i.length; a++ )
			i[a].start();
			return this
		 };
		 d.stop = function(){
			for(var a = 0; a < i.length; a++)
			i[a].stop();
			return this
		 };
		 d._triggerOnStart = function(){ c.trigger(a.eMoveStart,b) };
		 d._triggerOnMove = function(){ c.trigger(a.eMove,b) };
		 d._triggerOnStop = function(){ c.trigger(a.eMoveStop,b) };
		 
	var A={};
	(function(a){
		var b=null,
		c="mousemove";
		
		a.Hover = function(g,d,h){
			var b = g.extend({}, a.Hover.defaults, h),
			e = false,
			
			f = function(a){
				a=a[d.props.mouse] - b.el.offset()[d.props.edge];
				d.position((a-b.mouseStart)/(d.props.fSize(b.el)-b.mouseStart-b.mouseEnd));e=false
			};
			
			this.stop = function(){
				b.el.unbind( c, f );
				e = false};
				this.start = function(){
					e || ( e = true,b.el.on( c, f ) )
				};
				
				b.el = b.el ? g(b.el) : d.$mask};
				a.Hover.defaults = { el:b,
											mouseStart:50,
											mouseEnd:50
										};
				a.HoverCenter = function(l,e,o){
					var j="mouseleave",
					b=l.extend( {}, a.HoverCenter.defaults, o ),
					i = 0,
					f = false,
					h,
					d = false,
					n = function(){ e.moveBounded(i); g() },
					k = function(a){
						f = false; a = a[e.props.mouse] - b.el.offset()[e.props.edge] - (Math.round(e.props.fSize(b.el) / 2) + b.center);
						i = b.moveFunction ? b.moveFunction(a) : a < -b.mouseStart || a > b.mouseEnd ? -b.coefficient * (a+(0<a?-b.mouseEnd:b.mouseStart)):0;d||(n(),h=setInterval(n,b.timeout),d=true)
					},
					m=function(){
						i=0;
						d && (clearInterval(h), d=false);
						g();
						eventLeaveOn=false
					},
					g = function(){
						f||(b.el.one(c,k),f=true)
					};
					this.stop = function(){
						b.el.unbind(c,k);b.el.unbind(j,m);d&&(clearInterval(h),d=false)
					};
					this.start=function(){ g(); b.el.one( j, m ) };
					b.el = b.el ? l(b.el) : e.$mask
				};
				a.HoverCenter.defaults = {
					el:b,
					timeout:50,
					mouseStart:150,
					mouseEnd:150,
					center:0,
					coefficient:.2,
					moveFunction:b
				};
				a.Drag = function(d,c,n){
					var b=d.extend( {}, a.Drag.defaults, n ),
					i,
					f=false,
					e=0,
					j=0,
					h=false,
					k = function(a){
						b.prevent && a.preventDefault();
						f = true;
						i = a [c.props.mouse];
						e=0; d(document).on(b.eventMove,g);
						d(document).one(b.eventEnd,l);
						h=false;
						c.$mask.trigger("flowSliderDragStart",c)
					},
					g = function(a){
						a.preventDefault();
						if(f){
							j = e;
							e = (a[c.props.mouse]-i)*b.coefficient;var a=c.getOffset()+e-j,g=c.boundOffset(a);
							if(a!=g){
								var d=Math.abs(g-a),h=!b.outFunction?d*b.outCoefficient:b.outFunction(d);
								e+=g<a?h-d:d-h
							}
							c.move(e-j);
							b.el[0].fsDragging=true
						}
					},
					l=function(a){
						b.prevent&&a.preventDefault();c.$mask.unbind(b.eventMove,g);f=false;c.moveBounded((a[c.props.mouse]-i)*b.coefficient-e);d(document).unbind(b.eventMove,g);m();c.$mask.trigger("flowSliderDragEnd",c);setTimeout(function(){b.el[0].fsDragging=false},b._delay)
					},
					m=function(){
						h || (h=true,b.el.one(b.eventStart,k))
					};
					this.stop=function(){
						f&&(d(document).unbind(b.eventMove,g),d(document).unbind(b.eventEnd,l),c.offset(c.boundOffset(c.getOffset())),f=false,b.el[0].fsDragging=false);d(document).unbind(b.eventStart,k);h=false
					};
					this.start=function(){ m() };
					b.el=b.el ? d(b.el):c.$mask
				};
				a.Drag.defaults={
					el:b,eventStart:"mousedown",eventMove:c,eventEnd:"mouseup",coefficient:1,outCoefficient:.125,outFunction:b,_delay:200,prevent:false
				};
				a.Touch=function(d,b,q){
					var l="touchstart",k="touchend touchcancel",j="touchmove",c=d.extend({},a.Touch.defaults,q),m,e=0,i=0,f=false,g=false,n=function(a){c.prevent&&a.preventDefault();f=true;m=a.originalEvent.touches[0][b.props.mouse];e=0;d(document).on(j,h);d(document).one(k,o);g=false;b.$mask.trigger("flowSliderTouchStart",b)},h=function(a){a.preventDefault();if(f){i=e;e=(a.originalEvent.touches[0][b.props.mouse]-m)*c.coefficient;var a=b.getOffset()+e-i,g=b.boundOffset(a);if(a!=g){var d=Math.abs(g-a),h=!c.outFunction?d*c.outCoefficient:c.outFunction(d);e+=g<a?h-d:d-h}b.move(e-i);c.el[0].fsDragging=true}},o=function(a){c.prevent&&a.preventDefault();f=false;d(document).unbind(c.eventMove,h);b.offset(b.boundOffset(b.getOffset()));p();b.$mask.trigger("flowSliderTouchEnd",b);setTimeout(function(){c.el[0].fsDragging=false},c._delay)},p=function(){g||(g=true,c.el.one(l,n))};this.stop=function(){f&&(d(document).unbind(j,h),d(document).unbind(k,o),b.offset(b.boundOffset(b.getOffset())),f=false,c.el[0].fsDragging=false);d(document).unbind(l,n);g=false};this.start=function(){p()};c.el=c.el?d(c.el):b.$mask
				};
				a.Touch.defaults={
					el:b,
					timeout:50,
					coefficient:1,
					outCoefficient:.125,
					outFunction:b,
					_delay:200,
					prevent:false
				};
				a.Wheel=function(j,b,i){
					var f="mousewheel",c=j.extend({},a.Wheel.defaults,i),e=false,d=0,h=function(){var a=+new Date;a>=d+c.timeout?(b.offset(b.boundOffset(b.getOffset())),e=false):setTimeout(h,c.timeout-(a-d))},g=function(g,f){g.preventDefault();var a=f*c.speed;offset=b.getOffset()+a;bound=b.boundOffset(offset);offset!=bound&&(a=c.outFunction(f,Math.abs(bound-offset)));c.timeout?(b.move(a),d=+new Date,!e&&offset!=bound&&(setTimeout(h,c.timeout),e=true)):b.moveBounded(a)};this.stop=function(){b.$mask.unbind(f,g)};this.start=function(){b.$mask.bind(f,g)}
				};
				a.Wheel.defaults={
					speed:40,
					timeout:500,
					outFunction:function(b,a){return h<a?0:300*b/a}
					};
				a.Key=function(f,c,g){
					var b=f.extend({},a.Key.defaults,g),e=f(b.target),d=function(a){a.keyCode==b.keyFwd&&c.moveBounded(-b.step);a.keyCode==b.keyRev&&c.moveBounded(b.step)};this.stop=function(){e.unbind("keydown",d)};this.start=function(){e.keydown(d)}
				};
				a.Key.defaults={
					target:document,
					step:h,
					keyFwd:39,
					keyRev:37
				};
				a.Hash=function(g,c,e){
					var d=g.extend({},a.Hash.defaults,e),b=window.location.hash,f=function(){var a=c.getOffset();c.s.infinite&&(a%=c.calcSize()/2);window.location.hash=Math.round(-a/Math.abs(d.step))+1};this.stop=function(){};this.start=function(){b&&(b=parseInt(b.substr(1,b.length-1)),isNaN(b)&&(b=1),c.offset(c.boundOffset(-d.step*(b-1))));c.$mask.bind("flowSliderMoveStop",f)}
				};
				a.Hash.defaults={ step:h };
				a.Timer=function(c,g,f){
					var b=c.extend({},a.Timer.defaults,f),d,h=function(){g.moveBounded(-b.step)},e=function(){clearInterval(d)};this.stop=function(){e()};this.start=function(){c(b.el).bind(b.eventStart,function(){d=setInterval(h,b.time);if(b.eventEnd)c(b.el).one(b.eventEnd,e)})}};
				a.Timer.defaults={
					eventStart:"ready",
					eventEnd:b,
					el:document,
					step:h,
					time:3e3
					};
				a.Event=function(e,c,f){
					var b=e.extend({},a.Event.defaults,f),d=function(a){a.preventDefault();if(b.rewind){var a=c.getOffset(),d=c.boundOffset(a-b.step);if(a==d){c.position(0<-b.step?1:0);return}}c.moveBounded(-b.step)};this.stop=function(){b.el.unbind(b.event,d)};this.start=function(){b.el.bind(b.event,d)};b.el=b.el?e(b.el):c.$mask
				};
				a.Event.defaults={
					el:b,
					event:"click",
					step:h,
					rewind:true
				}
	})
	(A);
	
	var B={};
	(function(a){
		var b="px";a.None=function(c,b){
			var a;this.getOffset=function(){return a};this.offset=function(c){a=c;b.$wrap.css(b.props.edge,a)};this.shift=function(){};(a=parseInt(b.$wrap.css(b.props.edge)))||(a=0)
		};
		a.CSS=function(h,c,j){
			var e=h.extend({},a.CSS.defaults,j),f=parseInt(c.$wrap.css(c.props.edge)),i=c.props.edge,l=c.$wrap[0].style,g=true,d="",h=function(){var b="transition-timing-function",a={};a[d+"transition-property"]=c.props.edge;e.transitionAlt&&(a[d+b]=e.transitionAlt);a[d+"transition-duration"]=e.time+"ms";a[d+"transition-delay"]=e.delay+"ms";c.$wrap.css(a);c.$wrap.css(d+b,e.transition)},k=function(){g=true;c._triggerOnStop()};hookStopEvent=function(){c.$wrap.one("transitionend",k)};this.getOffset=function(){return f};this.offset=function(a){f=a;l[i]=f+b;g&&(g=false,hookStopEvent(),c._triggerOnStart())};(f=parseInt(c.$wrap.css(i)))||(f=0);d=FlowSlider.Util.transitionPrefix();false!==d&&h()
		};
		a.CSS.defaults={
			delay:-20,
			time:1e3,
			transitionAlt:"cubic-bezier(0.345, 1.0, 0.535, 0.795)",
			transition:"cubic-bezier(0.345, 1.650, 0.535, 0.795)"
		};
		a.Elastic=function(m,e,n){
			var g=m.extend({},
			a.Elastic.defaults,n),
			c,
			d,
			k,
			f=false,
			i=e.props.edge,
			j=e.$wrap[0].style,
			h,
			l,
			o=function(){
				h=(d-c)*g.elasticity;
				l=.5>Math.abs(h);d!=c&&!l?(c+=h,j[i]=c+b,e._triggerOnMove()):(g.snap&&(j[i]=d+b),f&&(clearInterval(k),f=false,e._triggerOnStop()))
			};
			this.getOffset=function(){
				return d
			};
			this.offset=function(a){
				d=a;f||(k=setInterval(o,g.frequency),f=true,e._triggerOnStart())
			};
			this.shift=function(a){ c+=a;d+=a };
			(c=parseInt( e.$wrap.css(i) )) || (c=0);
			d=c
		};
		a.Elastic.defaults={
			snap:false,frequency:25,elasticity:.25
		};
		a.Accelerating=function(n,f,o){
			var d=n.extend({},a.Accelerating.defaults,o),g,h,k,i=false,l=f.props.edge,p=f.$wrap[0].style,c=0,j,m,e,q=function(){(e=h-g)?(c=d.acc?c+(0<e?d.acc:-d.acc):d.speed,d.dec&&(j=Math.abs(c)/d.dec,m=d.dec*j*(j+1)/2,m>Math.abs(e-c)&&(c=(0<e?1:-1)*(-1+Math.sqrt(d.dec*(d.dec+8*Math.abs(e))))/2)),d.overshoot||(0>e&&0<c&&(c=-d.acc),0<e&&0>c&&(c=d.acc)),c>d.speed&&(c=d.speed),c<-d.speed&&(c=-d.speed),0<e&&c>e&&(c=e),0>e&&c<e&&(c=e),g+=c,p[l]=g+b,f._triggerOnMove()):(c=0,i&&(clearInterval(k),i=false,f._triggerOnStop()))};this.getOffset=function(){return h};this.offset=function(a){h=a;i||(k=setInterval(q,d.frequency),i=true,f._triggerOnStart())};this.shift=function(a){g+=a;h+=a};(h=g=parseInt(f.$wrap.css(l)))||(g=0)
		};
		a.Accelerating.defaults={
			frequency:50,
			speed:50,
			acc:3,
			dec:3,
			overshoot:true
		};
		a.jQuery=function(f,b,g){
			var d=f.extend({},a.jQuery.defaults,g),c,e=b.props.edge;this.getOffset=function(){return c};this.offset=function(a){c=a;a={};a[e]=c;b.$wrap.stop(true).animate(a,d.time,d.easing,b._triggerOnStop);b._triggerOnStart()};(c=parseInt(b.$wrap.css(e)))||(c=0)
		};
		a.jQuery.defaults={
			time:1e3,
			easing:"swing"
		};
		a.Transition=function(s,g,t){
			var c=s.extend({},a.Transition.defaults,t),o,m=false,h,e,d,p,n,l,i,j=0,f,k,q=g.props.edge,r=g.$wrap[0].style,u=function(){i=+new Date;n!=d&&(j=j>i?i+f+Math.abs(d-h)*c.timeCoefficient:i+Math.abs(d-h)*c.timeCoefficient-c.frequency/2,l=i+c.delay,f=j-l,f>c.timeMax&&(f=c.timeMax,j=l+f),f<c.timeMin&&(f=c.timeMin,j=l+f),h=e,p=d-h);i>=j?(e=d,r[q]=e+b,m&&(clearInterval(o),m=false,g._triggerOnStop())):(k=(i-l)/f,1<k&&(k=1),0>k&&(k=0),e=h+p*c.transition(k),r[q]=e+b,n=d,g._triggerOnMove())};this.getOffset=function(){return d};this.offset=function(a){d=a;m||(o=setInterval(u,c.frequency),m=true,g._triggerOnStart())};this.shift=function(a){e+=a;d+=a;h+=a;n+=a};(e=parseInt(g.$wrap.css(g.props.edge)))||(e=0);h=d=e
		};
		a.Transition.defaults={
			delay:-20,frequency:20,timeMin:h,timeMax:1500,timeCoefficient:10,transition:FlowSlider.Transition.circ
		}
	})
	(B);
	
	var G = function(){
		//c.append('<a href="http://www.flowslider.com" title="Flow Slider plugin&mdash;slide your HTML!" target="_blank" class="'+a.prefix+'-branding"></a>');
		var b = c.children("a."+a.prefix+"-branding");
	
		/*if(!FlowSlider.Global.brandingCSS){
		FlowSlider.Global.brandingCSS=true;
		var d=document.createElement("STYLE");
		d.type="text/css";
		var e="."+a.prefix+"-branding {background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAAARCAYAAAAi5qlcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0MTJEMUUwQ0IxMjlFMTExQTdENUE2NkI4MENGMEUyQSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3NjZGQ0EwNDQ3NkIxMUUxQjhBOUNEQUU5RTA3NzY4MiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3NjZGQ0EwMzQ3NkIxMUUxQjhBOUNEQUU5RTA3NzY4MiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IFdpbmRvd3MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMzRBQjk2MzZCNDdFMTExQThDOEFBQzQzREU4RDlCMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0MTJEMUUwQ0IxMjlFMTExQTdENUE2NkI4MENGMEUyQSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjyGfq4AAASpSURBVHja7FhdSBxXFD4z+6fRdsWEUrJUS1swJKhJaLp9iCJp4gaKPgqlWAxFQ8DkzSd91CehULRgkVKoSIkvBW0h2hSCW2Jt0aJv2pKiSZsmGrImJq7r7k7ud+OZ3J2dWVc67UPZA4c7c+69585837nnzFzNMAz6pxKNRo1QKERer5d0XZdaEHtJp9NSk8kk7ezskNcNpwDe7/fL1uPxFAjYg4BUKmVi5BoBUJ/PZ+6CgjgTgOjna1cIAOCIfCbivyZgcXGRampq/hXfKysrVFZWRsFg0LYPa6Ovvr4+bwK4BU5e7ev1KnF/Q+irOeYdE9omtMti3xTaOH9EN3M/6/LjNJ2LPqV7ceca8+vZEhpZ3aFPlhMZ9lKvRt+dLqZwuce0xWIx+bKqVFZWUlVVlbyOx+OugA0FmQC9sbGRpqenqb+/n/r6+uR6s7Oz5vjR0VHq7e2lnp4eamho2FfAsnoZ/M9PldoOvvjL5vei+eOLcOmVZDqrD5Om7OYx+E5+x1bi9HqJTjXlfjHGb/VL7/+4RcvnS6jcr5lRDkAADEd7a2ura5E+MDBAXV1dVFtbK8lWfWNNRDgIcFu8DD5eWsr8JNHJiDrmqtAzH89uBqz9u/NsEWbwnfwOnSyiG2tJavtpixz80pOkYRLAAvCnpl5w3t7ebl6PjIzIiEQUA7CxsTFqaWmRAOIaBDY1Nck+gK36QYRDJicn5XiQgOjn3QdlwRqDg4MZNlx3dHTQ+Pi4JArrIWgwjncXfFdXV2fuBo44BsHo/9CKJXbAOb6R/QBLnecgufy+94qHfrifov363djYkMAwOGr6ABkAF2kC/QAEAlB4zsTEhFQniUQitLCwIEmwrgmQmWSsg9TDAkKxztLSknwWEI8WvlAjQAr8ZKUjwn/ArpogvbD9aXxwaFW0p00bg6XMs5Ucfg8XEb12QKeb60nar1+8FACAqgJguC4gjTDwfM3jASQik+1mShQRC9ABWDgclnPthMnr7Oy0tfOOxHOycFDYFWpdexQjVn3oN2lUbDPCFhTtcbbJSWKcOs9Ocvl9t3ibNhJpWlx9QDn9Guksv5yC1PSRS7heAHSOaoBcV1eXMY5BYmI4dVhFTTt2dhRsfBDk+1Gge9bvkqq+3mn1/prQ80I9Dv1S7SSX38ghg67feki05rjuc7+pdN7FDLmdI5yjD4WUCQBAHLVcI6xFGKmiu7vb3El2ohJkjXKuJXgGpx2UVYS1tbs/i/Ydmz5Ux2+EfrWHj9+JXnrLany7KE5ztx9mDS72eaj5zRPUfnWetLW/HZ2+cVB8ARXl/z8BYIaHh2Uuxqdpc3OzjEY1l4MQpCKAq9p3j1MkeCAKYGIu8rhVQBDXE/XfAOO5DqAPPvL5N9ECbUPBXZBDlr5rQj8V+i0C2qkmCv3o5uVTdyoqKigQCJg/Yo+2EnTxyyj9FXuaMeHssRBdOnOUWj67LgLcPs+/XCw+TS/U0eGyA//bP+FEIkHb29ukuXEYNzc3Z4AAPg8qHEXkT4ArRxE4XIJTBr5AwN4EADPXzoKYAF5A0zSpBXE+DQVeUFdSEGRmZsbAgRy0QEBuAngXQJ8JMABn2v61okLcyAAAAABJRU5ErkJggg==);z-index:9999999 !important;padding:0 !important;display:block !important}";
	d.styleSheet?d.styleSheet.cssText=e:d.appendChild(document.createTextNode(e));
	g("head").append(d)
	}*/
	
		//b.css({
		//	margin:0,
		//	position:r,
		//	width:17,
		//	height:17,
		//	margin:0,
		//	opacity:.3,
		//	right:-20
		//	})
		//.delay(5e3).animate({ right:4 }, 1e3);
		
		//b.mouseenter(function(){
		//	b.stop().css({
		//		"background-position":"-17px 0",
		//		right:-58,
		//		width:79
		//	}).animate({ right:0 ,opacity:1 },h)
		//});
		
		//b.mouseleave(function(){
		//	b.stop().animate({ right:-58, opacity:.3 },
		//	{
		//		duration:h,
		//		complete:function(){
		//			b.css({ "background-position":"0 0", right:4, width:17 })
		//		}
		//	})
		//})
	},
	
	z = function(){
		a.detectTouchDevice && "ontouchstart" in document.documentElement && (a.controllers=["Touch"],a.controllerOptions=[a.touchOptions]);
		i=[];
		for ( var d, c = 0; c < a.controllers.length; c++)
			d = a.controllerOptions[c] ? a.controllerOptions[c] : a.controllerOptions[0],
			i[c] = new A[a.controllers[c]](g,b,d);
			a.detectCssTransition && "CSS" != a.animation && false !== FlowSlider.Util.transitionPrefix () && (a.animation="CSS",
			a.animationOptions = a.cssAnimationOptions);
			l = new B[a.animation]( g, b, a.animationOptions )
	},
	
	u=function(){
		var e="position";
		c.append(y+a.prefix+'-overlay-1"/><div class="'+a.prefix+'-overlay-2"/>');
		w=c.children("."+a.prefix+"-overlay-1");
		t=c.children("."+a.prefix+"-overlay-2");
		w.css(e,r).css(b.props.size,b.calcSize()).css(b.props.sizeC,b.props.fSizeC(c)).css(b.props.edge,0).css(b.props.edgeC,0);
		t.css(e,r).css(b.props.size,b.wrapSize).css(b.props.sizeC,b.props.fSizeC(c)).css(b.props.edgeEnd,0).css(b.props.edgeC,0);
		var d;
		c.mousemove(function(e){
			d=e[b.props.mouse]-c.offset()[b.props.edge];
			w.css(b.props.size,d-a.overlayPrecision);
			t.css(b.props.size,b.props.fSize(c)-d-a.overlayPrecision)
		})
	};
	
	FlowSlider.Global.sliders++;
	s===typeof a.onMoveStop&&c.bind(a.eMoveStop,a.onMoveStop);
	s===typeof a.onMoveStart&&c.bind(a.eMoveStart,a.onMoveStart);
	s===typeof a.onMove&&c.bind(a.eMove,a.onMove);
	d.props=q==a.mode?e:f;
	e=a.prefix+"-wrap";
	
	c.wrapInner('<span class="'+e+'-2" style="display:inline-block;"/>');
	c.wrapInner(y + e + '-1" style="position:absolute;"/>');
	m=g(c.children("."+e+"-1")[0]);
	j=g(m.children("."+e+"-2")[0]);
	"vertical" == a.mode&&j.css( o,"100%" );
	d.s = a;
	d.$mask=c;
	d.$wrap=m;
	c.addClass(a.prefix);
	d.setupDOM();
	//""!=location.host&&"localhost"!=location.host&&!location.host.match(/flowslider\.com/i)&&G();
	
	if(a.externalContent){
		e=false;
		for(f=0; f<a.controllers.length; f++)
			if("Hover"==a.controllers[f] || "HoverCenter" == a.controllers[f]){
				e=true;break
			}
		e && u()
	}
	
	m.css(d.props.edge,d.positionToOffset(a.startPosition));
	z();
	d.position(a.position);
	
	for(f=0; f < i.length; f++)
		i[f].start();
		FlowSlider.Util.ping()
};
		
		FlowSlider.defaults={
			mode:"horizontal",
			infinite:false,
			animation:"Elastic",
			animationOptions:{},
			controllers:["Hover"],
			controllerOptions:[{}],
			marginStart:25,
			marginEnd:25,
			startPosition:0,
			position:.5,
			externalContent:false,
			overlayPrecision:7,
			detectTouchDevice:true,
			touchOptions:{},
			detectCssTransition:false,
			cssAnimationOptions:{},
			moveIfSmaller:false,
			onMoveStart:null,
			onMoveStop:null,
			onMove:null,
			prefix:"www_FlowSlider_com",
			eMoveStart:"flowSliderMoveStart",
			eMoveStop:"flowSliderMoveStop",
			eMove:"flowSliderMove"
		};
			
		FlowSlider.Global={
			VERSION:"1.5.0",
			PACK:"Free",
			pinged:false,
			sliders:0
		};
			
		FlowSlider.Util={
			transitionPrefix:function(){
				var a=FlowSlider.Global;
				if(void 0===a.tPrefix){
					a.tPrefix=false;
					var d=(document.body||document.documentElement).style,
					c = {
						transition:"",
						MozTransition:"-moz-",
						WebkitTransition:"-webkit-",
						MsTransition:"-ms-",
						OTransition:"-o-"
					},
					b;
					for(b in c)
						if(void 0!==d[b]){
							a.tPrefix=c[b];
							break
						}
				}
				return a.tPrefix
			},
			
			protocol:function(){
				//return"https:" == document.location.protocol?"https:":"http:"
			},
			ping:function(){
				//var a = window.location.hostname;
				//if(!FlowSlider.Global.pinged && a && "localhost" != a && (FlowSlider.Global.pinged=true, a = "ping.flowslider.com",!(+new Date%1))){
				//	var b=document.createElement("script");
				//	b.type="text/javascript";
				//	b.src = FlowSlider.Util.protocol() + "//" +a+ "/" + FlowSlider.Global.VERSION + "/" + FlowSlider.Global.PACK;
				//	$("head")[0].appendChild(b)
				//}
			}
		};
				FlowSlider.Array=function(b){
					for( var c = "calcSize, getPosition, position, positionToOffset, getOffset, offset, boundOffset, setupDOM, content, set, start, stop".split(","), d=this, a=0; a<c.length; a++ )
						(function(a){
							d[a]=function(){
								for(var c=0;c<b.length;c++){
									var e=b[c].FlowSlider,
										 f=e[a].apply(e,arguments);
									if(void 0!==f && f!==e)
										return f
									}
								return d
							}
						})
					(c[a]);
					return this
				};
					
		FlowSlider.Transition = {
			none:function(){return 1},
			linear:function(a){return a},
			quadratic:function(a){ var b=a*a;return a*(-b*a+4*b-6*a+4) },
			cubic:function(a){ return a*(4*a*a-9*a+6) },
			elastic:function(a){ var b=a*a;return a*(33*b*b-106*b*a+126*b-67*a+15) },
			circ:function(a){ return Math.sqrt(1-(a-=1)*a) },
			CubicBezier:function(d,f,m,k,e,g){
				var n=e?e:.001,
					 o=g?g:5,
					 a=3*d,
					 b=3*(m-d)-a,
					 p=2*b,
					 h=1-a-b,
					 j=3*h,
					 c=3*f,
					 i=3*(k-f)-c,
					 l=1-c-i;
				this.transition=function(f){
					for(var d=f,g=0,e;g<o;g++){
						e=d*(a+d*(b+d*h))-f;
						if(Math.abs(e)<n)
							break;
							d-=e/(a+d*(p+d*j))
					}
					return d*(c+d*(i+d*l))
				}
			}
		};
		
		(function(a){
			a.fn.FlowSlider=function(b){
				return FlowSlider.Array( this.each(function(d,c){
					c.FlowSlider||(c.FlowSlider=new FlowSlider(c,b,a))
				}))
			}
		})
		
		(jQuery);